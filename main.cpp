#include <iostream>
#include <string>
#include <map>
#include <unistd.h>

#include "ScriptInfo.h"
#include "FileWalker.h"

const std::string appVersion = "1.2";


void printHelp() {
	std::cout << "Mage v" << appVersion << "\n\n";
	std::cout << "Usage:\n";
	std::cout << "  mage BUILTIN-COMMAND [PARAMS]...\n";
	std::cout << "  mage USER-SCRIPT [PARAMS]...\n\n";
	std::cout << "Built-in commands:\n";
	std::cout << "  help                   Print this help.\n";
	std::cout << "  version                Print application's version.\n";
	std::cout << "  list [-f]              List all available user scripts.\n";
	std::cout << "      -f, --full-path\n";
	std::cout << "          Print full path of the script\n";
	std::cout << "  whereis USER-SCRIPT    Print location of the given script.\n";
	std::cout << "  cat USER-SCRIPT        Print content of the given script file.\n\n";
	std::cout << "User scripts:\n";
	std::cout << "  Execute user supplied script regardless of the real location.\n";
	std::cout << "  For example:\n\n";
	std::cout << "    mage foobar.sh\n\n";
	std::cout << "Exit status:\n";
	std::cout << "  0 if OK,\n";
	std::cout << "  1 if no argument is given,\n";
	std::cout << "  2 if invalid arguments were given,\n";
	std::cout << "  3 if command cannot be executed,\n";
	std::cout << "  5 if command is not found or file cannot be opened.\n";
}

void printVersion() {
	std::cout << "Mage version " << appVersion << '\n';
}

void printExecError() {
	std::cerr << "-------------------------------------------------------\n";
	std::cerr << "| Error:                                               |\n";
	std::cerr << "| Ooops, unable to run the script!                     |\n";
	std::cerr << "|                                                      |\n";
	std::cerr << "|   -> Check if it is executable.                      |\n";
	std::cerr << "----------------------------------------------------\n";
}

void printNoSuchScriptAvailable() {
	std::cerr << "-------------------------------------------------------\n";
	std::cerr << "| Error:                                               |\n";
	std::cerr << "| No such script is available!                         |\n";
	std::cerr << "|                                                      |\n";
	std::cerr << "|   -> Check if it really exists.                      |\n";
	std::cerr << "----------------------------------------------------\n";
}

int main(int argc, char** argv) {
	if (argc == 1 || std::string("help") == argv[1]) {
		printHelp();
		return 1;
	}

	if (std::string("version") == argv[1] || std::string("-v") == argv[1]) {
		printVersion();
		return 0;
	}

	FileWalker fw = FileWalker();
	fw.init();

	if (std::string("list") == argv[1]) {
		bool fullPath = false;
		if (argc >= 3) {
			fullPath = std::string("-f") == argv[2] || std::string("--full-path") == argv[2];
		}
		fw.printList(fullPath);
		return 0;
	}

	if (std::string("whereis") == argv[1]) {
		if (argc != 3) {
			printHelp();
			return 2;
		}

		try {
			fw.printWhereis(argv[2]);
			return 0;
		} catch (const std::runtime_error& e) {
			std::cerr << "Error: " << e.what() << std::endl;
			return 5;
		}
	}

	if (std::string("cat") == argv[1]) {
		if (argc != 3) {
			printHelp();
			return 2;
		}

		try {
			fw.printContent(argv[2]);
			return 0;
		} catch (const std::runtime_error& e) {
			std::cerr << "Error: " << e.what() << std::endl;
			return 5;
		}
	}

	try {
		const ScriptInfo* info = fw.get(argv[1]);

		// Hm, ok. index 1 is mind fuck. Why isn't 0 included?
		if (execv(info->get_full_path().c_str(), &argv[1]) == -1) {
			printExecError();
			return 3;
		}
	} catch (std::out_of_range e) {
		printNoSuchScriptAvailable();
		return 5;
	}
}
