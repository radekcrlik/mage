CPP_FLAGS=--std=c++17 -O2
CC=g++

VERSION=1.2

APT_DIR=mage
OBJ_FILES=obj
DESTDIR=


main: $(OBJ_FILES)/main.o $(OBJ_FILES)/script_info.o $(OBJ_FILES)/file_walker.o
	$(CC) $(CPP_FLAGS) -o $@ $^ -lstdc++fs


$(OBJ_FILES)/main.o: main.cpp ScriptInfo.h FileWalker.h | $(OBJ_FILES)
	$(CC) $(CPP_FLAGS) -c main.cpp -o $(OBJ_FILES)/main.o

$(OBJ_FILES)/script_info.o: ScriptInfo.h ScriptInfo.cpp | $(OBJ_FILES)
	$(CC) $(CPP_FLAGS) -c ScriptInfo.cpp -o $(OBJ_FILES)/script_info.o

$(OBJ_FILES)/file_walker.o: FileWalker.h FileWalker.cpp ScriptInfo.h | $(OBJ_FILES)
	$(CC) $(CPP_FLAGS) -c FileWalker.cpp -o $(OBJ_FILES)/file_walker.o


mage_$(VERSION)_amd64.deb: main mage-completion.bash manpage control | $(APT_DIR)
	mkdir -p $(APT_DIR)/usr/bin
	cp main $(APT_DIR)/usr/bin/mage

	ln -f -s /usr/bin/mage $(APT_DIR)/usr/bin/mg

	mkdir -p $(APT_DIR)/DEBIAN
	cp control $(APT_DIR)/DEBIAN/control

	mkdir -p $(APT_DIR)/etc/bash_completion.d/
	cp mage-completion.bash $(APT_DIR)/etc/bash_completion.d/mage-completion.bash

	mkdir -p $(APT_DIR)/usr/share/man/man1
	cp manpage $(APT_DIR)/usr/share/man/man1/mage.1
	gzip $(APT_DIR)/usr/share/man/man1/mage.1
	cp $(APT_DIR)/usr/share/man/man1/mage.1.gz $(APT_DIR)/usr/share/man/man1/mg.1.gz

	dpkg-deb --build $(APT_DIR)
	mv ./mage.deb ./mage_$(VERSION)_amd64.deb

mage_$(VERSION)_amd64.deb.md5sum: mage_$(VERSION)_amd64.deb
	md5sum -b mage_$(VERSION)_amd64.deb > mage_$(VERSION)_amd64.deb.md5sum

.PHONY: apt
apt: mage_$(VERSION)_amd64.deb.md5sum


$(APT_DIR):
	mkdir $(APT_DIR)

$(OBJ_FILES):
	mkdir $(OBJ_FILES)


install:
	cp main $(DESTDIR)/usr/bin/mage
	ln -f -s $(DESTDIR)/usr/bin/mage $(DESTDIR)/usr/bin/mg
	cp mage-completion.bash $(DESTDIR)/etc/bash_completion.d/mage-completion.bash
	cp manpage $(DESTDIR)/usr/share/man/man1/mage.1
	gzip $(DESTDIR)/usr/share/man/man1/mage.1
	cp $(DESTDIR)/usr/share/man/man1/mage.1.gz $(DESTDIR)/usr/share/man/man1/mg.1.gz

uninstall:
	rm $(DESTDIR)/usr/bin/mg
	rm $(DESTDIR)/usr/bin/mage
	rm $(DESTDIR)/etc/bash_completion.d/mage-completion.bash
	rm $(DESTDIR)/usr/share/man/man1/mg.1.gz
	rm $(DESTDIR)/usr/share/man/man1/mage.1.gz


.PHONY: clean
clean:
	rm -f main
	rm -rf $(OBJ_FILES)
	rm -rf $(APT_DIR)
	rm -f *.deb
	rm -f *.md5sum
