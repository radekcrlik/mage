#/usr/bin/env bash

_traverse_mage_dirs()
{
	# Recursion base case
	[[ "$1" == "/" ]] && return
	[[ "$1" == "" ]] && return

	if [[ -d "$1/.mage" ]]; then
		SCRIPTS+=(`find $1/.mage -type f -executable -printf '%P '`)
	fi

	_traverse_mage_dirs `dirname $1`
}

_mage_completions()
{
	if [ "${#COMP_WORDS[@]}" == "2" ]; then
		SCRIPTS=()
		_traverse_mage_dirs `pwd`

		COMPREPLY=($(compgen -W "list help version whereis cat ${SCRIPTS[*]}" -- "${COMP_WORDS[1]}"))
	elif [ "${#COMP_WORDS[@]}" == "3" ]; then
		if [[ "${COMP_WORDS[1]}" == "whereis" || "${COMP_WORDS[1]}" == "cat" ]]; then
			SCRIPTS=()
			_traverse_mage_dirs `pwd`

			COMPREPLY=($(compgen -W "${SCRIPTS[*]}" -- "${COMP_WORDS[2]}"))
		fi
	fi
}

complete -F _mage_completions mage
complete -F _mage_completions mg
