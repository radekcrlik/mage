.TH mage 1 "07 Apr 2023" "mage v1.2" "mage man page"
.SH NAME
mage \- Simple shell script organizer 
.SH SYNOPSIS
.B mage BUILTIN-COMMAND [PARAMS]...

.B mage USER-SCRIPT [PARAMS]...

.SH DESCRIPTION
Mage is a simple command line utility for organizing and sharing scripts between directories.

.SH BUILT-IN COMAMNDS
.TP
.BR help
display help information and exit
.TP
.BR version
display application version and exit
.TP
.BR list
list all available user scripts
.TP
.BR "whereis USER-SCRIPT"
display location of the given script
.TP
.BR "cat USER-SCRIPT"
print content of the given script file to standard output

.SH USER SCRIPTS
Run user scripts by simply passing their filename.
These scripts can be written in any language and must be marked as executable.

For example:
    $ mage foo.sh
    $ mage bar.py

Or using an mg alias:
    $ mg foo.sh
    $ mg bar.py

.SH EXIT CODES
.TP
.BR 0 
ok
.TP
.BR 1
if no argument is given
.TP
.BR 2
if invalid arguments were given
.TP
.BR 3
if command cannot be executed
.TP
.BR 5
if command is not found or file cannot be opened

.SH EXAMPLES
With a following file organization:

/project
  /.mage
      - push.sh
  /main
      .mage/
          - foo.bar
  /hotfix
      .mage/
          - bar.sh
          - push.sh

You can list all available scripts in the main directory by running:

    $ cd /project/main
    $ mage list
 
Output:
    foo.sh  [/project/main/.mage]
    push.sh [/project/.mage]

 To execute those scripts you can type:
 
     $ mage foo.sh
     $ mage push.sh

This will execute /project/main/.mage/foo.sh and /project/.mage/push.sh. Current directory will be used as a working directory.

To list all files available for hotfix directory, type:

    $ cd /project/hotfix
    $ mage list -f

Output:
    bar.sh  [/project/hotfix/.mage/bar.sh]
    push.sh [/project/hotfix/.mage/push.sh]

Notice that push.sh is listed in hotfix/.mage directory now because it is closer to the current directory.
This way, you can override scripts for certain directories but have shareble scripts by putting them in parent .mage directories. 

.SH AUTHOR
Written by Radek Crlík.
.SH COPYRIGHT
do What The Fuck you want to Public License

This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
