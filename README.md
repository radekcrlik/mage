# Mage
Is a simple Linux shell script organizer.

# How does it work?

Create a directory named `.mage` and put your executable scripts there. For example, Bash scripts.
```
project/
   .mage/
      foo.sh
      bar.sh
```

You have all your scripts at one place now and you can easily exclude them via .gitignore (or .git/info/exclude), in case of git, all at once.
No more noise when running `git status`.

Now, when in the project directory, you can list all available scripts by running:
```
project $ mage list

List of commands:

  foo.sh    [/home/user/project/.mage]
  bar.sh    [/home/user/project/.mage]
```
You get the script names and where they are located (this will become important later). Mage looks for executable files only.

To execute the script run a following command:
```
project $ mage foo.sh
```
And you can even pass parameters and they will be passed to the script:
```
project $ mage foo.sh param1 param2
```
This is equivalent to:
```
project $ ./mage/foo.sh param1 param2
```

# Command Alias

There is also an alias for `mage` called `mg` for those that like to save some extra time typing :slight_smile:

So these commands are equivalent:
```
mage list
mg list
```

# Sharing scripts between directories

Let's say you have two projects in two directories and you have a bunch of scripts you would like to share for them.
You can move the `.mage` directory one level higher:
```
somedir/
   .mage/
     foo.sh
     bar.sh
   project-1/
   project-2/
```

If you run `mage list` in the directory of one of those projects you should see output similar to this:
```
project-1 $ mage list

List of commands:

  foo.sh    [/home/user/somedir/.mage]
  bar.sh    [/home/user/somedir/.mage]
```

Mage looks for all scripts in the .mage directories all the way up to the root.

# Overriding scripts

Of course, from time to time, you need to customize a script for some projects. Or have a specific script for one project only.
This is the main feature of mage. You can create more .mage directories in the current working directory or in parent directories. Look at an example:
```
somedir/
  .mage/
     foo.sh
     bar.sh
  project-1/
     .mage/
        foo.sh
  project-2/
```
Carefully notice the output of list command.

```
project-1 $ mage list

List of commands:

  foo.sh    [/home/user/somedir/project-1/.mage]
  bar.sh    [/home/user/somedir/.mage]
```
Script name `foo.sh` is now picked up from the project-1 directory. Mage always tries to find the script in the closest destination.

With all this, you can easily share and override scripts for your directories and execute them in a standard way via `mage` command.

# Bash tab completion

Mage also includes bash script that enables command-line tab completion. It will try to complete the script name no matter the location.

So if you write:
```
project1 $ mage ba
```
and hit TAB, you should get:
```
project1 $ mage bar.sh
```

# Compiling

To compile mage you need:
  - make
  - g++

Then, you can simply run: `make`

To install it on your system, you can use: `make install`

You can also remove it from your system by running: `make uninstall`

Or you can also install mage by building and using one of the packages: deb, snap


# Bugs and ideas
If you find a bug or you have an idea for a new features, simply use Gitlab issues to report it.

https://gitlab.com/radekcrlik/mage/-/issues

# License

do What The Fuck you want to Public License, Radek Crlík.

But if you find it useful and decide to fork and improve it I will be glad to hear about it!
