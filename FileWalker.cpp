#include "FileWalker.h"
#include <iostream>
#include <fstream>
#include <experimental/filesystem>
#include <unistd.h>

namespace fs = std::experimental::filesystem;

using std::cout;

void FileWalker::init() {
	std::string cur_path = fs::current_path();
	crawl_directory(cur_path);
}

void FileWalker::printList(const bool print_full_path) const noexcept {
	cout << "List of commands:\n\n";

	std::size_t longest = 0;
	for (const auto& [file, info] : file_map) {
		std::size_t str_size = info->get_file().size();
		if (longest < str_size) {
			longest = str_size;
		}
	}

	for (const auto& [file, info] : file_map) {
		std::size_t str_size = info->get_file().size();

		cout << "  " << info->get_file();
		for (int i = str_size; i < longest + 4; ++i) {
			cout << ' ';
		}


		if (print_full_path) {
			cout << '[' << info->get_full_path() << "]\n";
		} else {
			cout << '[' << info->get_path() << "]\n";
		}
	}
}

void FileWalker::printWhereis(const std::string& name) const {

	for (const auto& [file, info] : file_map) {
		if (file == name) {
			cout << info->get_full_path() << std::endl;
			return;
		}
	}

	throw std::runtime_error("Script not found!");
}

void FileWalker::printContent(const std::string& name) const {

	for (const auto& [file, info] : file_map) {
		if (file == name) {
			print_file_content(info->get_full_path());
			return;
		}
	}

	throw std::runtime_error("Script not found!");
}

ScriptInfo* FileWalker::get(const std::string& name) {
	return file_map.at(name);
}


void FileWalker::crawl_directory(const std::string &dir) {
	std::string path = dir + "/.mage";

	if (fs::exists(path) && fs::is_directory(path)) {
		for (const auto & entry : fs::directory_iterator(path)) {
			// Check if it's a regular file
			if (!fs::is_regular_file(entry.path())) {
				continue;
			}

			// Check if it's executable file
			if (access(entry.path().c_str(), X_OK)) {
				continue;
			}

			ScriptInfo* si = new ScriptInfo(entry.path());
			if (file_map.find(si->get_file()) == file_map.end()) {
				file_map[si->get_file()] = si;
			}
		}
	}

	if (dir == "/" || dir.empty()) {
		return;
	}

	std::size_t found = dir.find_last_of("/\\");
	std::string next_dir = dir.substr(0, found);

	crawl_directory(next_dir);
}

void FileWalker::print_file_content(const std::string &path) const {
	std::streampos size;
	char* memblock;

	std::ifstream file (path, std::ios::in | std::ios::binary | std::ios::ate);
	if (!file.is_open()) {
		throw std::runtime_error("Cannot open file for reading!");
	}

	size = file.tellg();
	memblock = new char[size + 1l];
	file.seekg (0, std::ios::beg);
	file.read (memblock, size);
	memblock[size] = 0;
	file.close();

	cout << memblock;

	delete[] memblock;
}
