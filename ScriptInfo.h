#ifndef _SCRIPT_INFO_H_
#define _SCRIPT_INFO_H_

#include <string>

class ScriptInfo {
public:
	ScriptInfo(const std::string &_full_path);

	const std::string get_file() const noexcept;
	const std::string get_path() const noexcept;
	const std::string get_full_path() const noexcept;

private:
	std::string file;
	std::string path;
	std::string full_path;
};

#endif
