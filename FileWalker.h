#ifndef _FILE_WALKER_H_
#define _FILE_WALKER_H_

#include <string>
#include <map>
#include "ScriptInfo.h"

class FileWalker {
public:
	void init();
	void printList(const bool print_full_path) const noexcept;
	void printWhereis(const std::string& name) const;
	void printContent(const std::string& name) const;
	ScriptInfo* get(const std::string& name);

private:
	std::map<std::string, ScriptInfo*> file_map;

	void crawl_directory(const std::string &dir);
	void print_file_content(const std::string &path) const;
};

#endif
