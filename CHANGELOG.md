# Version 1.2

## New features
- list command now provides long version of -f switch named --full-path
- bash code completion script added
- Linux man page added


# Version 1.1

## New features
- Option 'list' now supports -f switch to print full path of the command
- New 'whereis' option to print path to a given command
- New 'cat' option to print content of a given command
