#include <experimental/filesystem>
#include "ScriptInfo.h"

namespace fs = std::experimental::filesystem;

ScriptInfo::ScriptInfo(const std::string &_full_path) {
	std::size_t found = _full_path.find_last_of("/\\");
	path = _full_path.substr(0, found);
	file = _full_path.substr(found + 1);
	full_path = _full_path;
}

const std::string ScriptInfo::get_file() const noexcept {
	return file;
}

const std::string ScriptInfo::get_path() const noexcept {
	return path;
}

const std::string ScriptInfo::get_full_path() const noexcept {
	return full_path;
}
